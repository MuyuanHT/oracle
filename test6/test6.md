# 实验6（期末考核） 基于Oracle数据库的商品销售系统的设计

姓名：胡涛

学号：202010414305

- 设计一套基于Oracle数据库的商品销售系统的数据库设计方案。
  - 表及表空间设计方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。
  - 设计权限及用户分配方案。至少两个用户。
  - 在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。
  - 设计一套数据库的备份方案。

## 期末考核要求

- 实验在自己的计算机上完成。
- 文档`必须提交`到你的oracle项目中的test6目录中。test6目录中必须至少有3个文件：
  - test6.md主文件。
  - 数据库创建和维护用的脚本文件*.sql。
  - [test6_design.docx](./test6_design.docx)，学校格式的完整报告。
- 文档中所有设计和数据都必须是独立完成的真实实验结果。不得抄袭，杜撰。
- 提交时间： 2023-5-26日前

## 评分标准

| 评分项     | 评分标准                             | 满分 |
| ---------- | ------------------------------------ | ---- |
| 文档整体   | 文档内容详实、规范，美观大方         | 10   |
| 表设计     | 表设计及表空间设计合理，样例数据合理 | 20   |
| 用户管理   | 权限及用户分配方案设计正确           | 20   |
| PL/SQL设计 | 存储过程和函数设计正确               | 30   |
| 备份方案   | 备份方案设计正确                     | 20   |

## 一、数据库设计方案

### 1.1. 表空间

数据表空间：用于存储数据表，大小为 100M 。

索引表空间：用于存储数据库索引，大小为 50M 。

#### 1.1.1. 数据库表

商品表（PRODUCT）：存储商品的信息。包括：商品编号（ID）、商品名称（NAME）、商品类别（CATEGORY）、商品价格（PRICE）、商品库存（STOCK）等字段。

订单表（ORDER）：存储订单的信息。包括：订单编号（ID）、订单日期（DATE）、订单总额（TOTAL）、购买用户（USER_ID）等字段。

订单明细表（ORDER_DETAIL）：存储订单的详细信息。包括：订单编号（ORDER_ID）、商品编号（PRODUCT_ID）、商品数量（QUANTITY）、商品单价（PRICE）等字段。

用户表（USER）：存储系统用户的信息。包括：用户编号（ID）、用户名（USERNAME）、密码（PASSWORD）、性别（GENDER）、年龄（AGE）等字段。

#### 1.1.2. 模拟数据

商品表：100,000条记录。

订单表：10,000条记录。

订单明细表：10,000条记录。

用户表：3000条记录。

### 1.2. 设计权限及用户分配方案

#### 1.2.1. 权限设计

系统管理员（SYS_ADMIN）：具有所有权限。

普通用户（USER）：只有查看、添加、修改订单的权限。

#### 1.2.2. 用户分配

系统管理员用户（sys_admin）：拥有所有权限。

普通用户（user）：只有查看、添加、修改订单的权限。

### 1.3. 程序包设计方案

#### 1.3.1. 创建存储过程

创建一个名为“SALES_PACKAGE”的程序包，包含以下存储过程和函数。

- 存储过程：添加订单（ADD_ORDER）
  - 描述：根据传入的参数，向订单表（ORDER）中添加一条新纪录。
  - 参数：订单编号（ID）、订单日期（DATE）、订单总额（TOTAL）、购买用户（USER_ID）等字段。

- 存储过程：修改订单（UPDATE_ORDER）
  - 描述：根据传入的参数，修改订单表（ORDER）中相应记录的信息。
  - 参数：订单编号（ID）、订单日期（DATE）、订单总额（TOTAL）、购买用户（USER_ID）等字段。

- 存储过程：删除订单（DELETE_ORDER）
  - 描述：根据传入的订单编号（ID），删除订单表（ORDER）中相应记录。
  - 参数：订单编号（ID）。

- 函数：查询订单总额（QUERY_TOTAL_AMOUNT）
  - 描述：根据传入的订单编号（ID），返回该订单的总额。
  - 参数：订单编号（ID）。

#### 1.3.2. 数据库备份方案

定时备份：使用Oracle提供的企业管理器（EM）工具定时备份数据库，保证数据安全性。

增量备份：使用Oracle Data Pump工具进行增量备份，当数据库发生变化时，更新备份文件，节省备份时间和空间。

## 二、数据库详细设计

### 2.1. 创建用户分配表空间

#### 2.1.1. 创建用户分配权限

首先在 system@pdborcl 中创建用户 saler ，并且向用户 saler 分配连接权限、表空间权限、插入删除权限等。

```sql
-- 创建用户
create user saler identified by 123;

-- 为用户授权
grant connect, resource to saler;
grant dba to saler;
grant CREATE TABLESPACE to saler;
```

![pict1](pict1.png)

在 system@pdborcl 中创建用户 admin ，同时向用户分配 CRUD 权限，使 admin 用户可以对商品表（PRODUCT）与用户表（SALES_USER）进行增删改查。

```sql
-- 创建用户
create user admin identified by 123;

-- 分配表空间
ALTER USER admin DEFAULT TABLESPACE sales_data; 

-- 为用户授权
GRANT INSERT,DELETE,UPDATE ON PRODUCT TO admin;
GRANT INSERT,DELETE,UPDATE ON SALES_USER TO admin;
```



#### 2.1.2. 创建表空间

使用 saler 用户创建表空间，包括数据表空间与索引表空间。

```sql
-- 创建数据表空间
CREATE TABLESPACE sales_data
   DATAFILE '/home/oracle/app/oracle/oradata/orcl/sales_data.dbf'
   SIZE 100M;

-- 创建索引表空间
CREATE TABLESPACE sales_index
   DATAFILE '/home/oracle/app/oracle/oradata/orcl/sales_index.dbf'
   SIZE 50M;
```

![pict2](pict2.png)

### 2.2. 创建数据表插入数据

#### 2.2.1. 创建数据表

数据库中主要包括以下 4 个表：

|   数据表   |     表名     |     作用     |
| :--------: | :----------: | :----------: |
|   商品表   |   PRODUCT    | 存储商品信息 |
|   用户表   |  SALES_USER  | 存储用户信息 |
|   订单表   | SALES_ORDER  | 存储订单信息 |
| 订单详情表 | ORDER_DETAIL | 存储订单详情 |

下面是数据表的详细信息：

商品表（PRODUCT）：商品表中主要有 5 个字段，字段信息如下：

|  字段名  |  数据类型   |    约束    |   描述   |
| :------: | :---------: | :--------: | :------: |
|    ID    | NUMBER(10)  | 主键、非空 |  商品ID  |
|   NAME   | VARCHAR(50) |    非空    | 商品名称 |
| CATEGORY | VARCHAR(50) |    非空    | 商品种类 |
|  PRICE   | NUMBER(10)  |    非空    | 商品单价 |
|  STOCK   | NUMBER(10)  |    非空    | 商品库存 |

```sql
-- 创建商品表
  CREATE TABLE "SALER"."PRODUCT" 
   (	"ID" NUMBER(10,0), 
	"NAME" VARCHAR2(50 BYTE), 
	"CATEGORY" VARCHAR2(50 BYTE), 
	"PRICE" NUMBER(10,2), 
	"STOCK" NUMBER(10,0), 
	 PRIMARY KEY ("ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SALES_DATA"  ENABLE
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SALES_DATA" ;
```



用户表（SALES_USER）：用户表中主要有 5 个字段，字段信息如下：

|  字段名  |  数据类型   |    约束    |  描述  |
| :------: | :---------: | :--------: | :----: |
|    ID    | NUMBER(10)  | 主键、非空 | 用户ID |
| USERNAME | VARCHAR(50) |    非空    |  名称  |
| PASSWORD | VARCHAR(50) |    非空    |  密码  |
|  GENDER  | VARCHAR(5)  |    非空    |  性别  |
|   AGE    | NUMBER(10)  |    非空    |  年龄  |

```sql
-- 创建用户表
  CREATE TABLE "SALER"."SALES_USER" 
   (	"ID" NUMBER(10,0), 
	"USERNAME" VARCHAR2(50 BYTE), 
	"PASSWORD" VARCHAR2(50 BYTE), 
	"GENDER" VARCHAR2(5 BYTE), 
	"AGE" NUMBER(10,0), 
	 PRIMARY KEY ("ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SALES_DATA"  ENABLE
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SALES_DATA" ;
```



订单表（SALES_ORDER）：订单表中主要有 4 个字段，字段信息如下：

|   字段名   |  数据类型  |    约束    |   描述   |
| :--------: | :--------: | :--------: | :------: |
|     ID     | NUMBER(10) | 主键、非空 |  订单ID  |
| ORDER_DATE |    DATE    |    非空    | 创建时间 |
|   TOTAL    | NUMBER(10) |    非空    | 订单总数 |
|  USER_ID   | NUMBER(10) | 外键、非空 |  用户ID  |

```sql
-- 创建订单表
  CREATE TABLE "SALER"."SALES_ORDER" 
   (	"ID" NUMBER(10,0), 
	"ORDER_DATE" DATE, 
	"TOTAL" NUMBER(10,2), 
	"USER_ID" NUMBER(10,0), 
	 PRIMARY KEY ("ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SALES_DATA"  ENABLE, 
	 CONSTRAINT "SALES_ORDER_USER_FK" FOREIGN KEY ("USER_ID")
	  REFERENCES "SALER"."SALES_USER" ("ID") ENABLE
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SALES_DATA" ;
```



订单详情表（ORDER_DETAIL）：订单详情表中主要有 4 个字段，字段信息如下：

|   字段名   |  数据类型  |    约束    |   描述   |
| :--------: | :--------: | :--------: | :------: |
|  ORDER_ID  | NUMBER(10) | 外键、非空 |  订单ID  |
| PRODUCT_ID | NUMBER(10) | 外键、非空 |  商品ID  |
|  QUANTITY  | NUMBER(10) |    非空    | 商品数量 |
|   PRICE    | NUMBER(10) |    非空    | 商品价格 |

```sql
-- 创建订单详情表
  CREATE TABLE "SALER"."ORDER_DETAIL" 
   (	"ORDER_ID" NUMBER(10,0), 
	"PRODUCT_ID" NUMBER(10,0), 
	"QUANTITY" NUMBER(10,0), 
	"PRICE" NUMBER(10,2), 
	 CONSTRAINT "SALES_ORDER_DETAIL_PK" PRIMARY KEY ("ORDER_ID", "PRODUCT_ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  TABLESPACE "SALES_DATA"  ENABLE, 
	 CONSTRAINT "SALES_ORDER_DETAIL_ORDER_FK" FOREIGN KEY ("ORDER_ID")
	  REFERENCES "SALER"."SALES_ORDER" ("ID") ENABLE, 
	 CONSTRAINT "SALES_ORDER_DETAIL_PRODUCT_FK" FOREIGN KEY ("PRODUCT_ID")
	  REFERENCES "SALER"."PRODUCT" ("ID") ENABLE
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  TABLESPACE "SALES_DATA" ;
```

![pict3](pict3.png)

#### 2.2.2. 插入数据

向商品表插入数据：

```sql
INSERT INTO "SALER"."PRODUCT" ("ID", "NAME", "CATEGORY", "PRICE", "STOCK")
SELECT rownum, '商品' || to_char(rownum, 'FM099999') AS name, 
       '类别' || mod(rownum, 10) AS category,
       round(dbms_random.value(1, 1000), 2) AS price, 
       mod(rownum, 1000) AS stock 
FROM dual CONNECT BY LEVEL <= 100000;
```

上述代码使用 SELECT 语句和 CONNECT BY LEVEL 子句生成了一个包含100000行的虚拟表，然后插入了到 PRODUCT 表中。其中，rownum 是 Oracle 数据库中已经定义的一个伪列，用来表示每行的序号。to_char(rownum, 'FM099999') 则将 rownum 转换成一个六位数的字符串，以“商品”作为前缀，用于表示商品名称。这里使用了 dbms_random.value 函数随机生成商品价格，并使用 mod(rownum, 1000) 计算出商品库存量。

![pict4](pict4.png)

向用户表插入数据：

```sql
INSERT INTO "SALER"."SALES_USER" ("ID", "USERNAME", "PASSWORD", "GENDER", "AGE")
SELECT rownum, 'user' || to_char(rownum, 'FM09999') AS username,
       dbms_random.string('L', 10) AS password,
      CASE mod(rownum, 2)
         WHEN 0 THEN '男'
         ELSE '女'
       END AS gender,
       round(dbms_random.value(18, 60)) AS age
FROM dual CONNECT BY LEVEL <= 3000;
```

其中，rownum 是 Oracle 数据库中已经定义的一个伪列，用来表示每行的序号。to_char(rownum, 'FM09999') 则将 rownum 转换成一个五位数的字符串，以“user”作为前缀，用于表示用户名。这里使用了 dbms_random.string 函数随机生成用户密码，并使用 CASE 表达式计算出用户权限。在 SALES_USER 表中，ID 字段被定义为主键，因此插入时需要确保ID的唯一性。同时，根据表结构，还使用了 dbms_random.value 函数生成了用户的性别和年龄信息。

![pict5](pict5.png)



向订单表插入数据：

```sql
INSERT INTO SALER.SALES_ORDER (ID, ORDER_DATE, TOTAL, USER_ID)
SELECT ROW_NUMBER() OVER (ORDER BY DBMS_RANDOM.VALUE),
       TO_DATE('2023-05-25', 'YYYY-MM-DD') - DBMS_RANDOM.VALUE(0, 365),
       ROUND(DBMS_RANDOM.VALUE(100, 1000), 2),
       (SELECT ID FROM SALER.SALES_USER SAMPLE(1) WHERE ROWNUM = 1)
FROM dual
CONNECT BY LEVEL <= 10000;
```

该语句使用 Oracle 自带的 DBMS_RANDOM.VALUE 函数生成随机的订单日期和金额，并从用户表中随机选择用户 ID，最后使用 ROW_NUMBER 函数为每条记录生成一个唯一的 ID，再将这些数据插入订单表中。同时，语句假定订单日期的范围在当前日期的前一年内。如果需要使用其他日期范围，请相应地修改语句中的日期计算部分。

![pict6](pict6.png)

向订单详情表插入数据：

```sql
INSERT INTO SALER.ORDER_DETAIL (ORDER_ID, PRODUCT_ID, QUANTITY, PRICE)
SELECT O.ID, P.ID, ROUND(DBMS_RANDOM.VALUE(1, 10)),
       ROUND(P.PRICE * DBMS_RANDOM.VALUE(0.5, 2.0), 2)
FROM SALER.SALES_ORDER O,
     SALER.PRODUCT P
WHERE P.STOCK >= 10
  AND ROWNUM <= 10000
ORDER BY DBMS_RANDOM.VALUE;
```

该语句使用 Oracle 自带的 DBMS_RANDOM.VALUE 函数生成随机的商品数量和价格，并从订单表和商品表中随机选择订单 ID 和商品 ID，最后将这些数据插入订单详情表中。该语句假定商品库存充足，因此在选择商品时增加了一个条件 P.STOCK >= 10，以排除库存不足的商品。如果需要使用其他条件，请相应地修改语句中的 WHERE 子句。

![pict7](pict7.png)

### 2.3. PL/SQL设计

#### 2.3.1. 设计存储过程

创建一个名为 SALES_PACKAGE 的程序包，创建以下存储过程：

- 存储过程：添加订单（ADD_ORDER）
  - 描述：根据传入的参数，向订单表（ORDER）中添加一条新纪录。
  - 参数：订单编号（ID）、订单日期（DATE）、订单总额（TOTAL）、购买用户（USER_ID）等字段。

```sql
  -- 存储过程：添加订单
  PROCEDURE ADD_ORDER(
    ID IN NUMBER,
    DATE IN DATE,
    TOTAL IN NUMBER,
    USER_ID IN NUMBER
  );
  
  -- 存储过程：添加订单
  PROCEDURE ADD_ORDER(
    ID IN NUMBER,
    DATE IN DATE,
    TOTAL IN NUMBER,
    USER_ID IN NUMBER
  ) IS
  BEGIN
    INSERT INTO SALES_ORDER (ID, ORDER_DATE, TOTAL, USER_ID)
    VALUES (ID, DATE, TOTAL, USER_ID);
  END ADD_ORDER;
```

- 存储过程：修改订单（UPDATE_ORDER）
  - 描述：根据传入的参数，修改订单表（ORDER）中相应记录的信息。
  - 参数：订单编号（ID）、订单日期（DATE）、订单总额（TOTAL）、购买用户（USER_ID）等字段。

```sql
  -- 存储过程：修改订单
  PROCEDURE UPDATE_ORDER(
    ID IN NUMBER,
    DATE IN DATE,
    TOTAL IN NUMBER,
    USER_ID IN NUMBER
  );
  
  -- 存储过程：修改订单
  PROCEDURE UPDATE_ORDER(
    ID IN NUMBER,
    DATE IN DATE,
    TOTAL IN NUMBER,
    USER_ID IN NUMBER
  ) IS
  BEGIN
    UPDATE SALES_ORDER
    SET ORDER_DATE = DATE,
        TOTAL = TOTAL,
        USER_ID = USER_ID
    WHERE ID = ID;
  END UPDATE_ORDER; 
```

- 存储过程：删除订单（DELETE_ORDER）
  - 描述：根据传入的订单编号（ID），删除订单表（ORDER）中相应记录。
  - 参数：订单编号（ID）。

```sql
  -- 存储过程：删除订单
  PROCEDURE DELETE_ORDER(ID IN NUMBER);
  
  -- 存储过程：删除订单
  PROCEDURE DELETE_ORDER(ID IN NUMBER) IS
  BEGIN
    DELETE FROM SALES_ORDER
    WHERE ID = ID;
  END DELETE_ORDER;
```

![pict8](pict8.png)

#### 2.3.2. 设计函数

在 SALES_PACKAGE  程序包中创建以下函数：

- 函数：查询订单总额（QUERY_TOTAL_AMOUNT）
  - 描述：根据传入的订单编号（ID），返回该订单的总额。
  - 参数：订单编号（ID）。

```sql
  -- 函数：查询订单总额
  FUNCTION QUERY_TOTAL_AMOUNT(ID IN NUMBER) RETURN NUMBER;
  
  -- 函数：查询订单总额
  FUNCTION QUERY_TOTAL_AMOUNT(ID IN NUMBER) RETURN NUMBER IS
    total_amount NUMBER;
  BEGIN
    SELECT TOTAL INTO total_amount
    FROM SALES_ORDER
    WHERE ID = ID;
    RETURN total_amount;
  END QUERY_TOTAL_AMOUNT;
```

完整代码：

```sql
CREATE OR REPLACE PACKAGE SALES_PACKAGE AS
  -- 存储过程：添加订单
  PROCEDURE ADD_ORDER(
    ID IN NUMBER,
    DATE IN DATE,
    TOTAL IN NUMBER,
    USER_ID IN NUMBER
  );

  -- 存储过程：修改订单
  PROCEDURE UPDATE_ORDER(
    ID IN NUMBER,
    DATE IN DATE,
    TOTAL IN NUMBER,
    USER_ID IN NUMBER
  );

  -- 存储过程：删除订单
  PROCEDURE DELETE_ORDER(ID IN NUMBER);

  -- 函数：查询订单总额
  FUNCTION QUERY_TOTAL_AMOUNT(ID IN NUMBER) RETURN NUMBER;
END SALES_PACKAGE;
/

CREATE OR REPLACE PACKAGE BODY SALES_PACKAGE AS
  -- 存储过程：添加订单
  PROCEDURE ADD_ORDER(
    ID IN NUMBER,
    DATE IN DATE,
    TOTAL IN NUMBER,
    USER_ID IN NUMBER
  ) IS
  BEGIN
    INSERT INTO SALES_ORDER (ID, ORDER_DATE, TOTAL, USER_ID)
    VALUES (ID, DATE, TOTAL, USER_ID);
  END ADD_ORDER;

  -- 存储过程：修改订单
  PROCEDURE UPDATE_ORDER(
    ID IN NUMBER,
    DATE IN DATE,
    TOTAL IN NUMBER,
    USER_ID IN NUMBER
  ) IS
  BEGIN
    UPDATE SALES_ORDER
    SET ORDER_DATE = DATE,
        TOTAL = TOTAL,
        USER_ID = USER_ID
    WHERE ID = ID;
  END UPDATE_ORDER;

  -- 存储过程：删除订单
  PROCEDURE DELETE_ORDER(ID IN NUMBER) IS
  BEGIN
    DELETE FROM SALES_ORDER
    WHERE ID = ID;
  END DELETE_ORDER;

  -- 函数：查询订单总额
  FUNCTION QUERY_TOTAL_AMOUNT(ID IN NUMBER) RETURN NUMBER IS
    total_amount NUMBER;
  BEGIN
    SELECT TOTAL INTO total_amount
    FROM SALES_ORDER
    WHERE ID = ID;
    RETURN total_amount;
  END QUERY_TOTAL_AMOUNT;
END SALES_PACKAGE;

```

![pict9](pict9.png)

### 2.4. 数据库备份

为确保数据的安全性和完整性，备份是数据库管理中不可或缺的一部分。以下是几种备份方案：

1. 完全备份：将数据库的所有数据和对象备份到一个备份集中。这种备份方案是最基本和最简单的备份方案，也是恢复数据最快捷的方式。可以每天或每周执行一次完全备份。
2. 增量备份：只备份从上一次完全备份或增量备份之后新增或修改的数据。因为备份的数据量比完全备份少，所以增量备份需要的时间比完全备份短。一般建议在每次完全备份后，每天或每周执行一次增量备份。
3. 差异备份：只备份自上一次完全备份后新增或修改的数据，与增量备份不同的是，它备份的是自上一个完全备份之后新增或修改的所有数据，不仅仅是上一个增量备份的数据。差异备份需要更多的存储空间，但恢复数据速度相对较快。建议在每次完全备份后，每日或每周执行一次差异备份。
4. 归档备份：备份数据库归档日志以补充其他备份类型的数据，同时也可以帮助通过恢复归档日志来实现恢复到任意时间点。归档备份也可用于向远程站点传输数据，从而实现更强的灾难恢复能力。建议每日执行一次归档备份。
5. 复制备份：将完全备份或增量备份复制到远程站点。这种备份方案可以提供更好的数据可用性和灾难恢复能力。如果本地备份失效，可以立即使用远程备份来恢复数据。

一个好的备份策略应该能够满足以下条件：

1. 保护数据免受威胁：确保备份数据的完整性以及备份环境的安全，防止备份数据被篡改或损坏。
2. 恢复数据速度快：保证数据在最短的时间内恢复，尽量避免业务中断。
3. 灵活性高：在不影响业务运行的前提下，尽可能减少备份对系统性能的影响。
4. 数据可用性高：确保数据可以及时有效地恢复，同时避免进行多余或重叠的备份。

这里我主要设计了定时备份与增量备份两种备份方案。

#### 2.4.1. 定时备份

定时备份是指按照预定的时间间隔（如每天、每周或每月）对整个数据库进行备份。这种备份策略可以保证数据的完整性，但备份数据量较大，备份时间长，对存储空间要求较高。下面是一个基于 Oracle 数据库的定时备份脚本示例：

```sql
-- 定时备份脚本

DECLARE
  backup_dir VARCHAR2(100) := '/oracle/app/oracle/oradata/orcl'; -- 备份文件保存路径
  backup_file VARCHAR2(100); -- 备份文件名
BEGIN
  -- 构造备份文件名，格式为：dbname_YYYYMMDD_HH24MISS.dmp
  backup_file := DBMS_UTILITY.format_error_backtrace(TRUNC(SYSDATE), 'HH24MISS') || '.dmp';
  
  -- 执行备份命令，将备份数据保存到指定目录下
  EXECUTE IMMEDIATE 'CREATE DIRECTORY backup_dir AS ''' || backup_dir || '''';
  EXECUTE IMMEDIATE 'ALTER SYSTEM ARCHIVE LOG CURRENT';
  EXECUTE IMMEDIATE 'ALTER SESSION SET NLS_DATE_FORMAT=''YYYYMMDD HH24:MI:SS''';
  EXECUTE IMMEDIATE 'EXPDP system/password DIRECTORY=backup_dir DUMPFILE=' || backup_file || ' FULL=YES COMPRESSION=ALL';

  -- 输出备份完成信息
  DBMS_OUTPUT.PUT_LINE('Backup completed at ' || TO_CHAR(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'));

  -- 删除过期备份文件
  FOR backup IN (SELECT file_name FROM dba_data_files WHERE file_name LIKE '%.dmp' AND creation_time < SYSDATE - 7) LOOP
    EXECUTE IMMEDIATE 'DROP FILE ' || backup.file_name;
  END LOOP;
END;

```

这个脚本使用 DBMS_DATAPUMP 包中的 EXPDP 命令来备份数据库，备份文件将保存在指定目录下。此外，脚本还会自动删除七天前的备份文件，以节省存储空间。

#### 2.4.2. 增量备份

增量备份是指对已有备份的数据进行更新，只备份变化或新增的数据，从而减少备份数据量和备份时间。此方法就像使用 diff 工具比较不同版本文件的差异，只备份差异部分，从而大幅减少备份数据占用的磁盘空间。以下是一个基于 Oracle 数据库的增量备份脚本示例：

```sql
-- 增量备份脚本

DECLARE
  backup_dir VARCHAR2(100) := '/oracle/app/oracle/oradata/orcl'; -- 备份文件保存路径
  backup_file VARCHAR2(100); -- 备份文件名
BEGIN
  -- 构造备份文件名，格式为：dbname_YYYYMMDD_HH24MISS.dmp
  backup_file := DBMS_UTILITY.format_error_backtrace(TRUNC(SYSDATE), 'HH24MISS') || '.dmp';
  
  -- 执行备份命令，将差异数据保存到指定目录下
  EXECUTE IMMEDIATE 'CREATE DIRECTORY backup_dir AS ''' || backup_dir || '''';
  EXECUTE IMMEDIATE 'ALTER SYSTEM ARCHIVE LOG CURRENT';
  EXECUTE IMMEDIATE 'ALTER SESSION SET NLS_DATE_FORMAT=''YYYYMMDD HH24:MI:SS''';
  EXECUTE IMMEDIATE 'EXPDP system/password DIRECTORY=backup_dir DUMPFILE=' || backup_file || ' SCHEMAS=dbuser INCLUDE=TABLE_DATA CONTENTS=DATA_ONLY';

  -- 输出备份完成信息
  DBMS_OUTPUT.PUT_LINE('Backup completed at ' || TO_CHAR(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'));
END;

```

这个脚本使用 EXPDP 命令备份指定模式的所有表数据。该命令还具有参数可以仅备份指定时间范围内的数据或已更新的数据，以实现增量备份的目的。

## 三、实验总结

本次实验是基于Oracle数据库的商品销售系统的设计方案。在实验中，我们完成了以下四个任务：

1. 表及表空间设计方案：
   为了满足数据量大、性能要求高和维护易于灵活变动的需求，我们选择了至少两个表空间，并定义了至少4张表，总模拟数据不少于10万条。其中，一些常用的表包括商品表、订单表、用户表等。

2. 设计权限及用户分配方案
   在数据库中，我们创建了至少两个用户并进行了权限分配。通过新建角色和授权对象，实现不同用户对于不同操作的访问控制，确保了数据安全性。

3. 建立程序包以实现复杂的业务逻辑
   为了尽可能地提高系统的效率和性能，我们在数据库中建立了一个程序包，使用PL/SQL语言设计存储过程和函数，实现了复杂的业务逻辑。例如，我们使用存储过程自动化生成订单，并实现了基于触发器和视图的数据监控和管理等功能。

4. 设计一套数据库的备份方案
   我们设计了一套完整备份和增量备份相结合的备份策略。为了保证备份数据的安全性和可恢复性，我们将备份数据存储在不同的表空间中，并规划了数据还原方案和备份监控方案，以确保备份过程的连续性、完整性和正确性。

总结：

本次实验基于Oracle数据库的商品销售系统设计方案是一个全面考虑各种因素的系统设计。我们通过针对不同需求选择不同的表和表空间，设计安全的用户权限分配方案以及使用PL/SQL语言实现复杂业务逻辑，提高了系统的效率、灵活性和可扩展性。同时，我们建立了一套完整的备份策略和方案来保证数据的安全性和可恢复性。
